//
//  ViewController.swift
//  Athlytics
//
//  Created by Michael Merrill on 10/16/16.
//  Copyright © 2016 Michael Merrill. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController {
    
    @IBOutlet weak var heartRateLabel: UILabel!
    var manager: CBManager!
    var peripheral: CBPeripheral!
    
    var heartRate = 0 {
        didSet {
            heartRateLabel.text = String(heartRate)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        startManager()
    }
}

// MARK: CBCentralManagerDelegate

extension ViewController: CBCentralManagerDelegate {
    
    func startManager() {
        manager = CBCentralManager(delegate: self, queue: nil)
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            central.scanForPeripherals(withServices: nil, options: nil)
        default:
            print("Bluetooth not available")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if let deviceName = advertisementData[CBAdvertisementDataLocalNameKey] as? String {
            if !deviceName.isEmpty {
                central.stopScan()
                self.peripheral = peripheral
                central.connect(peripheral, options: [CBConnectPeripheralOptionNotifyOnDisconnectionKey : true])
            }
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        central.stopScan()
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Connection failed: \(error)")
    }
    
    func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
        // TODO
        print(#function)
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        disconnectPeripheral()
        print("Did disconnect with error: \(error)")
        startManager()
    }
}

extension ViewController: CBPeripheralDelegate {
    func disconnectPeripheral() {
        if self.peripheral != nil {
            self.peripheral.delegate = nil
            self.peripheral = nil
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else {
            print("No services found for \(peripheral.name)")
            return
        }
        
        for service in services {
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
        // TODO
        print(#function)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        // TODO
        print(#function)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
        // TODO
        print(#function)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
        // TODO
        print(#function)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?) {
        // TODO
        print(#function)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        // 0 - UUID = Heart Rate - 0x180D
        // - 0 : <CBCharacteristic: 0x1740b5180, UUID = 2A37, properties = 0x10, value = <16356c04>, notifying = NO>
        // - 1 : <CBCharacteristic: 0x1740b5960, UUID = 2A38, properties = 0x2, value = <01>, notifying = NO>
        if service.uuid == CBUUID(string: "180D") {
            if let heartRate = service.characteristics?.first {
                peripheral.setNotifyValue(true, for: heartRate)
            }
        }
        
        
        // 1 - UUID = Device Information - 0x180A
        // - 0 : <CBCharacteristic: 0x1700b6f80, UUID = System ID, properties = 0x2, value = <baf1c0fe ffd02200>, notifying = NO>
        // - 1 : <CBCharacteristic: 0x1700b70a0, UUID = Model Number String, properties = 0x2, value = <483700>, notifying = NO>
        // - 2 : <CBCharacteristic: 0x1700b71c0, UUID = Serial Number String, properties = 0x2, value = <32303136 43304631 424100>, notifying = NO>
        // - 3 : <CBCharacteristic: 0x1700b74c0, UUID = Firmware Revision String, properties = 0x2, value = <312e342e 3000>, notifying = NO>
        // - 4 : <CBCharacteristic: 0x1700b7460, UUID = Hardware Revision String, properties = 0x2, value = <38313035 31313331 ffffff00>, notifying = NO>
        // - 5 : <CBCharacteristic: 0x1700b7400, UUID = Software Revision String, properties = 0x2, value = <48372033 2e312e30 00>, notifying = NO>
        // - 6 : <CBCharacteristic: 0x1700b73a0, UUID = Manufacturer Name String, properties = 0x2, value = <506f6c61 7220456c 65637472 6f204f79 00>, notifying = NO>
        
        // 2 - UUID = Battery - 0x180F
        // - 0 : <CBCharacteristic: 0x1700b6e60, UUID = Battery Level, properties = 0x2, value = (null), notifying = NO>
        
        // 3 - UUID = 6217FF4B-FB31-1140-AD5A-A45545D7ECF3
        // - 0 : <CBCharacteristic: 0x1700b75e0, UUID = 6217FF4C-C8EC-B1FB-1380-3AD986708E2D, properties = 0x2, value = (null), notifying = NO>
        // - 1 : <CBCharacteristic: 0x1700b76a0, UUID = 6217FF4D-91BB-91D0-7E2A-7CD3BDA8A1F3, properties = 0x28, value = (null), notifying = NO>
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?) {
        // TODO
        print(#function)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        // TODO
        print(#function)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.characteristic.heart_rate_measurement.xml
        if let value = characteristic.value {
            let array = value.withUnsafeBytes {
                [UInt8](UnsafeBufferPointer(start: $0, count: value.count))
            }
            heartRate = Int(array[1])
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
        // TODO
        print(#function)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        // TODO
        print(#function)
    }
}

// MARK: CBPeripheralManagerDelegate

extension ViewController: CBPeripheralManagerDelegate {
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        // TODO
        print(#function)
    }
    
    func peripheralManagerIsReady(toUpdateSubscribers peripheral: CBPeripheralManager) {
        // TODO
        print(#function)
    }
    
    func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
        // TODO
        print(#function)
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest) {
        // TODO
        print(#function)
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, willRestoreState dict: [String : Any]) {
        // TODO
        print(#function)
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error: Error?) {
        // TODO
        print(#function)
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]) {
        // TODO
        print(#function)
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didSubscribeTo characteristic: CBCharacteristic) {
        // TODO
        print(#function)
    }
    
    func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didUnsubscribeFrom characteristic: CBCharacteristic) {
        // TODO
        print(#function)
    }
}
